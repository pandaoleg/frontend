export default class API {
  constructor() {
    this.key = '';
    this.method = '';
    this.param = '{}';
  }

  setApiParams(key, method, param = {}) {
    this.key = key;
    this.method = method;
    this.param = JSON.stringify(param);
  }

  getBodyString() {
    return `key=${this.key}&method=${this.method}&param=${this.param}`;
  }

  getFormDataHeaders() {

  }

  getUrlEncodedHeaders() {
    return {
      'Content-Type': 'application/x-www-form-urlencoded',
    }
  }
}

