module.exports = {
  authSuccess: (userData) => {
    return dispatch => {
      dispatch({type: 'AUTH_SUCCESS', payload: {authorized: true, info: userData}});
    };
  },

  logOut: () => {
    return dispatch => {
      dispatch({ type: 'LOG_OUT' });
    }
  },

  validateHash: (user) => {
    return dispatch => {
      const API = require('../lib/api').default;
      const hash = require('object-hash')(user.info);
      const api = new API();
      api.setApiParams(user.info.token, 'Security#Hash', { hash: hash });
      fetch(process.env.API_HOST, {
        method: 'POST',
        headers: api.getUrlEncodedHeaders(),
        body: api.getBodyString()
      }).then(response => {
        if (response.status === 500) {
          dispatch({ type: 'LOG_OUT' });
          return;
        } else if (response.status === 200) return;

        response.json().then(data => {
          if (response.status === 203) dispatch({type: 'AUTH_SUCCESS', payload: {authorized: true, info: data.data}});
          else dispatch({ type: 'LOG_OUT' });
        });
      });
    };
  }
}