import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools  } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import reducers from '../reducers';

const cachedInfo = window.localStorage.getItem('pm-store') ? JSON.parse(window.localStorage.getItem('pm-store')) : {};
const composeEnhancers = composeWithDevTools({});
const store = createStore(reducers, cachedInfo, composeEnhancers(applyMiddleware(thunk)));

// Save store state to localStorage
store.subscribe(() => {
  let state = Object.assign({}, store.getState());
  window.localStorage.setItem('pm-store', JSON.stringify(state));
});

export default store;