import React, { Component } from 'react';
import cn from 'cn-decorator';
import { Container, Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import AuthShape from '../components/Shapes/AuthShape';
import Card from '../components/Core/Card';
import APIMessage from '../components/Core/APIMessage';
import '../styles/auth.scss';

// Import authorize form's
import LoginForm from '../components/Forms/LoginForm';
const userAction = require('../actions/userActions');

@cn('authorize')
class authorize extends Component {
  state = {
    currentForm: 'login',
    message: null
  };

  /**
   * Method for getting form's title by type in state
   */
  _getFormTitle = () => {
    switch(this.state.currentForm) {
      case 'login': return 'Авторизация';
      case 'register': return 'Регистрация';
      default: return 'Авторизация';
    }
  };

  /**
   * Method for getting a form by type in state
   */
  _getFormByType = () => {
    switch(this.state.currentForm) {
      case 'login': {
        return <LoginForm onAuthTypeChange={this.onAuthTypeChange} onAuthSuccess={this.onAuth} onAuthFailed={this.onFailed}/>
      }

      default: {
        return <LoginForm onAuthTypeChange={this.onAuthTypeChange} onAuthSuccess={this.onAuth} onAuthFailed={this.onFailed}/>
      }
    }
  };

  /**
   * Handle changing form type
   * @param { string } currentForm The type of form
   */
  onAuthTypeChange = (currentForm) => {
    this.setState({ currentForm });
  };

  /**
   * Handle the submit of auth
   */
  onAuth = (data) => {
    this.props.authSuccess(data);
  };

  onFailed = (message) => { 
    this.setState({ message });
  };

  render(cn) {
    return (
      <Container className={cn()} fluid>
        <Row>
          <AuthShape />
          <Col className='d-none d-sm-block' sm={3} md={4}/>

          <Col className={cn('middle')} xs={12} sm={8} md={4}>
            <APIMessage message={this.state.message} />
            <Card className={cn('card')}>
              {this._getFormByType()}
            </Card> 
          </Col>

          <Col className='d-none d-sm-block' sm={3} md={4}/>
        </Row>
      </Container>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  
  dispatch => ({
    authSuccess: (userData) => {
      dispatch(userAction.authSuccess(userData));
      window.location.href = `${process.env.BASE_URL}player/`;
    }
  })
)(authorize);