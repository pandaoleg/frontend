import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Redirect } from 'react-router';
import { Container, Row, Col } from 'reactstrap';
import {withRouter} from 'react-router-dom';
import cn from 'cn-decorator';

import LeftMenu from '../components/Player/LeftMenu';
import PlayerControl from '../components/Player/PlayerControl';
import { Tracks } from '../components/Player/View';

import '../styles/player.scss';
const API = require('../lib/api').default;
window.Amplitude = require('amplitudejs/dist/amplitude');

@cn('player')
class player extends Component {
  state = {
    currentMenuOpened: 'tracks',
    tracks: [],
    albums: [],
    playlists: [],
    currentSongIndex: -1,
    payloadAmplitude: {}
  };

  onMenuChanged = (newMenu) => {
    this.setState({currentMenuOpened: newMenu});
  };

  /**
   * Get content view by currentMenuOpened state
   */
  _getCurrentView = () => {
    switch(this.state.currentMenuOpened.toLowerCase()) {
      case 'tracks': return (<Tracks tracks={this.state.tracks}/>);
      default: return (<Tracks tracks={this.state.tracks}/>);
    }
  }

  _getControlProperties = () => {
    return {
      onSettingChanged: this.onSaveSettings,
      settings: this.props.user.info.setting,
      track: this.state.tracks[this.state.currentSongIndex],
      amplitudePayload: this.state.payloadAmplitude
    }
  }

  /**
   * Method for getting the music cache from server
   */
  initPlayerCache = () => {
    const api = new API();
    api.setApiParams(this.props.user.info.token, 'Track#GetTracks');
    fetch(process.env.API_HOST, {
      method: 'POST',
      headers: api.getUrlEncodedHeaders(),
      body: api.getBodyString()
    }).then(response => {
      if (response.status === 500) {
        return;
      }

      response.json().then(data => {
        if (response.status === 200) {
          this.setState({tracks: data.data.tracks, albums: data.data.albums, playlists: data.data.playlists});

          let ampltitute = {
            songs: data.data.tracks.map(track => {
              return {
                name: track.title,
                artist: track.artist,
                album: track.album,
                url: track.fileUrl,
                cover_art_url: track.thumbnail
              }
            }),

            albums: {},

            callbacks: {
              song_change: () => {
                this.setState({currentSongIndex: window.Amplitude.getActiveIndex()});
              },

              after_play: () => {
                const payload = {
                  isPlaying: true
                };

                this.setState({payloadAmplitude: payload, currentSongIndex: window.Amplitude.getActiveIndex()});
              },

              after_pause: () => {
                const payload = {
                  isPlaying: false
                };

                this.setState({payloadAmplitude: payload});
              }
            },

            volume: this.props.user.info.setting.volume
          };

          for (let album of data.data.albums) ampltitute.albums[album.name] = album.index;
          window.Amplitude.init(ampltitute);
        }
      });
    });
  };

  onSaveSettings = (settings) => {
    const api = new API();
    api.setApiParams(this.props.user.info.token, 'User#SaveSetting', {
      settings: {
        volume: settings.volume
      }
    });

    fetch(process.env.API_HOST, {
      method: 'POST',
      headers: api.getUrlEncodedHeaders(),
      body: api.getBodyString()
    }).then(response => {
      if (response.status === 500) {
        return;
      }

      if (response.status !== 200) {
        response.json().then(data => {
          console.warn(data.message.message);
        });
      }
    });
  };

  componentDidMount() {
    this.initPlayerCache();
  }

  render(cn) {
    return (
      <div className={cn()} img='../img/Background.jpg'>
        <Container fluid>
          <Row style={{margin: 0}}>
            <Col xs={2}>
              <LeftMenu payload={{playlists: this.state.playlists}} onMenuChanged={this.onMenuChanged} />
            </Col>

            <Col xs={10}>
              <Container fluid className='right-container'>
                <Row style={{margin: 0}}>
                  {this._getCurrentView()}
                </Row>

                <Row style={{margin: 0}}>
                  <Col xs={12}>
                    <PlayerControl {...this._getControlProperties()} />
                  </Col>
                </Row>
              </Container>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default withRouter(connect(
  state => ({
    user: state.user
  }),
  
  dispatch => ({
    
  })
)(player));