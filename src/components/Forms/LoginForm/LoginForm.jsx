import React, { Component } from 'react';
import cn from 'cn-decorator';
import { PasswordInput, TextInput } from '../../Core/Input';
import { Container, Row, Col } from 'reactstrap';
import { BlueButton, WhiteButton } from '../../Core/Button';
import { GoogleLogin } from 'react-google-login';
import { Link } from 'react-router-dom'
import Form from '../../Core/Form';
import './LoginForm.scss';
import { User, Lock } from 'react-feather';
import API from '../../../lib/api';

@cn('login-form')
export default class LoginForm extends Component {
  state = {
    isLoading: false
  };

  handleAuth = async (type, additionalInformation) => {
    this.setState({ isLoading: true });

    switch(type) {
      case 'Google': {
        await this.onAuthWithGoogleSuccess(additionalInformation);
        break;
      }

      case 'Primary': {
        await this.primaryAuth();
        break;
      }

      case 'GoogleFailed': {
        await this.onAuthWithGoogleFailed(additionalInformation);
        break;
      }
    }

    this.setState({ isLoading: false });
  };

  /**
   * Handling primary authorizing
   */
  primaryAuth = async () => {
    const api = new API();
    api.setApiParams('', 'Security#Auth', { login: this.login.value, password: this.password.value });
    fetch(process.env.API_HOST, {
      method: 'POST',
      headers: api.getUrlEncodedHeaders(),
      body: api.getBodyString()
    }).then(response => {
      if (response.status === 500) {
        return;
      }

      response.json().then(data => {
        if (response.status === 200) this.props.onAuthSuccess(data.data);
        else this.props.onAuthFailed(data.message);
      });
    });
  };

  /**
   * Handle on google auth success
   * @param { object } googleAccount An object with google account data
   */
  onAuthWithGoogleSuccess = async (googleAccount) => {
    const api = new API();
    api.setApiParams('', 'Security#AuthGoogle', { token: googleAccount.tokenId });
    fetch(process.env.API_HOST, {
      method: 'POST',
      headers: api.getUrlEncodedHeaders(),
      body: api.getBodyString()
    }).then(response => {
      if (response.status === 500) {
        return;
      }

      response.json().then(data => {
        if (response.status === 200) this.props.onAuthSuccess(data.data);
        else this.props.onAuthFailed(data.message);
      });
    });
  }

  /**
   * Handle on google auth failed
   * @param { object } error An object with error information
   */
  onAuthWithGoogleFailed = async (error) => {
    this.props.onAuthFailed({type: 'error', message: error.details});
  };

  _setLoginRef = (input) => this.login = input;
  _setPasswordRef = (input) => this.password = input;

  _getGoogleSignInStyle = () => {
    return {
      width: 31, 
      height: 31,
      marginRight: 30,
      marginLeft: 0,
    }
  }

  _getGoogleProps = () => {
    return {
      clientId: '601378698565-pimt6miafsngp0aovq98con18t0fg44h.apps.googleusercontent.com',
      buttonText: 'Войти с помощью Google',
      className: 'button button--white',
      onSuccess: (account) => { this.handleAuth('Google', account) },
      onFailure: (err) => { this.handleAuth('GoogleFailed', err) }
    }
  }

  render(cn) {
    return (
      <div className={cn()}>
        <Row>
          <div className={cn('title')}>Авторизация</div>
          <Form className={cn('form')} preventSubmit onSubmit={() => { this.handleAuth('Primary') }}>
            <TextInput inputRef={this._setLoginRef} icon={(<User/>)} placeholder='Логин'/>
            <PasswordInput inputRef={this._setPasswordRef}  icon={(<Lock/>)} placeholder='Пароль'/>
            <BlueButton isLoading={this.state.isLoading} text='Войти'/>
          </Form>
        </Row>

        <Row>
          <GoogleLogin {...this._getGoogleProps()}>
            <img style={this._getGoogleSignInStyle()} src={`${process.env.BASE_URL}img/icons/google.svg`} alt='google-auth' /> Войти с помощью Google
          </GoogleLogin>

          <div className={cn('try-register')}>
            Еще нет аккаунта? <strong onClick={() => { this.props.onAuthTypeChange('register') }}>Зарегистрироваться</strong>
          </div>
        </Row>
      </div>
    );
  }
}
