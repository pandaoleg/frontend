import React, { Component } from 'react';
import cn from 'cn-decorator';
import { Container, Row, Col } from 'reactstrap';
import BluredThumbnail from '../Controls/ThumbnailShadow';
import VolumeControl from '../Controls/VolumeControl';
import CurrentTrackControl from '../Controls/CurrentTrackControl';
import './PlayerControl.scss';

@cn('player-control')
export default class PlayerControl extends Component {
  render(cn) {
    const getCurrentTrackInfoCol = () => {
      if (this.props.track) {
        return (
          <Col className={cn('info')} xs={4} md={3}>
            <BluredThumbnail src={this.props.track.thumbnail}/>

            <div>
              <h4 className={cn('title')}>{this.props.track.title}</h4>
              <div className={cn('artist')}>{this.props.track.artist}</div>
            </div>
          </Col>
        );
      }

      return null;
    };

    return (
      <Container fluid style={{padding: '0 15px'}} className={cn()}>
        <Row style={{alignItems: 'center', height: '100%'}}>
          {getCurrentTrackInfoCol()}

          <Col className={cn('panel')} xs={this.props.track ? 5 : 9} md={this.props.track ? 6 : 9}>
            <CurrentTrackControl amplitudePayload={this.props.amplitudePayload}/>
          </Col>

          <Col className={cn('volume')} xs={3}>
            <VolumeControl onVolumeChanged={this.props.onSettingChanged} volume={this.props.settings.volume}/>
          </Col>
        </Row>
      </Container>
    );
  }
}