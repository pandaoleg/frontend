import React, { Component } from 'react';
import cn from 'cn-decorator';
import { Repeat, SkipBack, PlayCircle, PauseCircle, SkipForward, Shuffle } from 'react-feather';
import './CurrentTrackControl.scss';

@cn('current-track')
export default class CurrentTrackControl extends Component {
  state = {
    isPlaying: this.props.amplitudePayload.isPlaying,
    isShuffle: false
  };

  componentWillReceiveProps(nextProps) {
    this.setState({isPlaying: nextProps.amplitudePayload.isPlaying});
  }

  play = () => {
    this.setState({isPlaying: true}, () => {
      window.Amplitude.play();
      const trackContainer = document.querySelectorAll('.track')[window.Amplitude.getActiveIndex()];
      trackContainer.classList.add('amplitude-playing');
      trackContainer.classList.remove('amplitude-paused');
    });
  };

  pause = () => {
    this.setState({isPlaying: false}, () => {
      window.Amplitude.pause();
      const trackContainer = document.querySelectorAll('.track')[window.Amplitude.getActiveIndex()];
      trackContainer.classList.remove('amplitude-playing');
      trackContainer.classList.add('amplitude-paused');
    });
  }

  toggleShuffle = () => {
    this.setState({isShuffle: !this.state.isShuffle});
    window.Amplitude.setShuffle();
  };

  render(cn) {
    const getPlayButton = () => {
      return this.state.isPlaying ? 
        (<PauseCircle onClick={this.pause} className={cn('play')}/>) :
        (<PlayCircle onClick={this.play} className={cn('play')}/>)
    };

    return (
      <div className={cn()}>
        <Repeat className={cn('repeat')}/>
        <SkipBack onClick={() => { window.Amplitude.prev() }} className={cn('back')}/>
        {getPlayButton()}
        <SkipForward onClick={() => { window.Amplitude.next() }} className={cn('next')}/>
        <Shuffle onClick={this.toggleShuffle} className={cn('shuffle', {random: this.state.isShuffle})}/>
      </div>
    );
  }
}
