import React, { Component } from 'react';
import cn from 'cn-decorator';
import Slider from 'react-rangeslider'
import { Volume, Volume1, Volume2, VolumeX } from 'react-feather';
import './VolumeControl.scss';
import './SliderStyle.scss';
let count = 0;

@cn('volume-control')
export default class VolumeControl extends Component {
  state = {
    volume: this.props.volume || 50,
    countSaved: 0
  }

  componentWillReceiveProps(newProps) {
    this.setState({volume: newProps.volume});
  }

  handleVolumeChange = (newVolume) => {
    this.setState({volume: newVolume});
    window.Amplitude.audio().volume = newVolume / 100;
  };

  saveVolume = () => {
    // there is checking for count becasue lib has a bug
    // this function called 2 time in a row
    if (count === 0) {
      this.props.onVolumeChanged({volume: this.state.volume});
      count++;
    } else count = 0;
  };

  render(cn) {
    const getIconDependsOnVolume = () => {
      const { volume } = this.state;
      if (volume === 0) return (<VolumeX />);
      else if (volume < 30) return (<Volume />);
      else if (volume < 60) return (<Volume1 />);
      else if (volume <= 100) return (<Volume2 />);
    };

    return (
      <div className={cn()}>
        {getIconDependsOnVolume()}
        <Slider tooltip={false}
                min={0} 
                max={100} 
                step={1} 
                value={+this.state.volume} 
                onChange={this.handleVolumeChange}
                onChangeComplete={this.saveVolume} />
      </div>
    );
  }
}
