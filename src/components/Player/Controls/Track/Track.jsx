import React, { Component } from 'react';
import cn from 'cn-decorator';
import { MoreHorizontal, Play, Pause } from 'react-feather';
import './Track.scss';
const secToMin = require('../../../../lib/secToMin');

@cn('track')
export default class Track extends Component {
  render(cn) {
    return (
      <div className={cn() + ' amplitude-play-pause amplitude-paused'} amplitude-song-index={this.props.index}>
        <img className={cn('cover')}src={this.props.track.thumbnail} alt='track-cover'/>
        <div className={cn('play')}><Play /></div>
        <div className={cn('title')}><span>{this.props.track.title}</span></div>
        <div className={cn('artist')}><span>{this.props.track.artist}</span></div>
        <div className={cn('album')}><span>{this.props.track.album}</span></div>
        <div className={cn('duration')}><span>{secToMin(this.props.track.duration)}</span></div>
        <MoreHorizontal />
      </div>
    );
  }
}
