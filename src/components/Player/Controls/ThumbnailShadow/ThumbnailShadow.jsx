import React, { Component } from 'react';
import cn from 'cn-decorator';
import './ThumbnailShadow.scss';

@cn('thumbnail-shadow')
export default class ThumbnailShadow extends Component {
  render(cn) {
    return (
      <div className={cn()}>
        <img src={this.props.src} alt="cover"/>
        <img src={this.props.src} alt="cover"/>
      </div>
    );
  }
}

