import React, { Component } from 'react';
import cn from 'cn-decorator';
import './MenuGroup.scss';

@cn('menu-group')
export default class MenuGroup extends Component {
  render(cn) {
    return (
      <div className={cn()}>
        <h4 className={cn('title')}>{this.props.title}</h4>
        <div className={cn('list')}>{this.props.children}</div>
      </div>
    );
  }
}
