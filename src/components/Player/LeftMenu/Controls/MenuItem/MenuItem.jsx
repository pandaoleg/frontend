import React, { Component } from 'react';
import cn from 'cn-decorator';
import './MenuItem.scss';

@cn('menu-item')
export default class MenuItem extends Component {
  render(cn) {
    return (
      <div onClick={this.props.onClick} className={cn({active: this.props.activeItem, hideLeft: this.props.hideActiveBlock})}>
        {this.props.icon}
        {this.props.text}
      </div>
    );
  }
}
