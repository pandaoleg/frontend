import React, { Component } from 'react';
import { connect } from 'react-redux';
import cn from 'cn-decorator';
import MenuItem from './Controls/MenuItem';
import MenuGroup from './Controls/MenuGroup';
import { Music, Mic, Disc, PlusCircle, Hash } from 'react-feather';
import './LeftMenu.scss';

@cn('left-menu')
class LeftMenu extends Component {
  state = {
    currentText: 'Треки'
  };

  handleChange = (currentText, translated) => {
    this.props.onMenuChanged(translated);
    this.setState({currentText: currentText});
  };

  /**
   * Method for printing the playlists inforamtion
   */
  _printPlaylists = () => {
    if (this.props.payload.playlists && this.props.payload.playlists.length > 0) {
      return (this.props.payload.playlists.map(playlist => (
        <MenuItem onClick={() => {}}
                  key={playlist.id}
                  activeItem={this.state.currentText === playlist.name}
                  
                  icon={(<Hash />)} 
                  text={playlist.name} />
      )));
    }

    return null
  };

  render(cn) {
    return (
      <div className={cn()}>
        <MenuGroup title='Меню'>
          <MenuItem onClick={() => { this.handleChange('Треки', 'Tracks') }} 
                    activeItem={this.state.currentText === 'Треки'} 
                    icon={(<Music />)} 
                    text='Треки' />

          <MenuItem onClick={() => { this.handleChange('Исполнители', 'Artists') }} 
                    activeItem={this.state.currentText === 'Исполнители'}
                    icon={(<Mic />)}
                    text='Исполнители' />

          <MenuItem onClick={() => { this.handleChange( 'Альбомы', 'Albums') }} 
                    activeItem={this.state.currentText === 'Альбомы'}
                    icon={(<Disc />)}
                    text='Альбомы' />
        </MenuGroup>

        <MenuGroup title='Плейлисты'>
          {this._printPlaylists()}
          <MenuItem onClick={() => {}} activeItem hideActiveBlock icon={(<PlusCircle />)} text='Новый плейлист' />
        </MenuGroup>
      </div>
    );
  }
}

export default connect(
  state => ({
    user: state.user
  }),
  
  dispatch => ({
    
  })
)(LeftMenu);