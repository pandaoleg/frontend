import React, { Component } from 'react';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom';
import { Col } from 'reactstrap';
import Track from '../../Controls/Track';
import cn from 'cn-decorator';
import InfiniteScrolle from 'react-infinite-scroll-component';
import InfiniteScroll from 'react-infinite-scroller';
import './Tracks.scss';

@cn('tracks')
class Tracks extends Component {
  state = {
    tracks: this.props.tracks || [],
    printedItems: [],
    offset: 10,
    height: 0,
    last: true
  };

  componentWillReceiveProps(nextProps) {
    this.setState({tracks: nextProps.tracks, printedItems: nextProps.tracks.slice(0, 10), offset: 10}, () => {
      this.setState({height: document.querySelector('.tracks').clientHeight - 100});
    });
  }

  printMoreTracks = () => {
    const { tracks, offset, printedItems } = this.state;
    let newTracks = tracks.slice();
    newTracks = newTracks.splice(offset, 10);
    this.setState({printedItems: printedItems.concat(newTracks), offest: offset + 10}, () => {
      if (this.state.printedItems.length === this.state.tracks.length) this.setState({last: false});
    });
  };

  _getScrollProperties = () => {
    let scrollOptions = {
      pageStart: 0,
      loadMore: this.printMoreTracks,
      hasMore: this.state.last,
      loader: null,
      useWindow: false,
      useCapture: true,
      threashold: 250
    }

    return scrollOptions;
  }

  render(cn) {
    return (
      <Col xs={12} className={cn()}>
        {
          this.state.tracks.length > 0 ?
            <InfiniteScroll {...this._getScrollProperties()}>
              <h4>Все треки</h4>
              <div className={cn('list')}>
                {this.state.printedItems.map((track, index) => (<Track key={index} index={index} track={track}/>))}
              </div>
            </InfiniteScroll>
          : null
        }
      </Col>
    );
  }
}


export default withRouter(connect(
  state => ({
    user: state.user
  }),
  
  dispatch => ({
    
  })
)(Tracks));