import React, { Component } from 'react';
import cn from 'cn-decorator';
import './TracksTableLegend.scss';

@cn('tracks-legend')
export default class TracksTableLegend extends Component {
  render(cn) {
    return (
      <div className={cn()}>
        <div><span>#</span></div>
        <div><span>Название</span></div>
        <div><span>Исполнитель</span></div>
        <div><span>Альбом</span></div>
        <div><span>Длительность</span></div>
      </div>
    );
  }
}

