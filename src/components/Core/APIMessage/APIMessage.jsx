import React, { Component } from 'react';
import cn from 'cn-decorator';
import './APIMessage.scss';

@cn('api-message')
export default class APIMessage extends Component {
  render(cn) {
    if (this.props.message) {
      return (
        <div className={cn({type: this.props.message.type.toLowerCase()})}>
          <span className={cn('text')}>{this.props.message.message}</span>
        </div>
      );
    }

    return null;
  }
}
