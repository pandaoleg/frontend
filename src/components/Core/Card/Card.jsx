import React, { Component } from 'react';
import cn from 'cn-decorator';
import './Card.scss';

@cn('card')
export default class Card extends Component {
  render(cn) {
    return (
      <div className={cn() + ' ' + (this.props.className || '')}>
        {this.props.title ? <div className={cn('title')}> {this.props.title} </div> : null}
        {this.props.children}
      </div>
    );
  }
}