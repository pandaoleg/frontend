import React, { Component } from 'react';

export default class Form extends Component {
  _validateForm = () => {
    let inputsElement = document.querySelectorAll(`.${this.props.className} input`);
    let inputsProps = this.props.children.filter(element => !!element.props.inputRef);
    
    inputsProps.forEach((value, index) => {
      // If input has required properties and input are empty
      // Then print error under input
      if (value.props.required && !inputsElement[index].value) {

      }

      // If input has regex validating in props and test is not success
      // Then print error under input
      if (value.props.regex && !value.props.regex.test(inputsElement[index].value)) {

      }
    });

    return true;
  };

  _handleSubmit = (e) => {
    if (this.props.preventSubmit) e.preventDefault();

    if (this._validateForm()) {
      this.props.onSubmit && this.props.onSubmit(e);
    }
  };

  render() {
    console.log(this.props.children);

    return (
      <form className={'form ' + (this.props.className || '')} onSubmit={this._handleSubmit}>
        {this.props.children}
      </form>
    );
  }
}