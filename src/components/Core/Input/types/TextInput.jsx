import React, { Component } from 'react';
import Input from '../Input';

export default class TextInput extends Component {
  render() {
    return (
      <Input type='text' {...this.props}/>
    );
  }
}
