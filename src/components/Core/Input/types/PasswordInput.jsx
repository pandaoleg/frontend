import React, { Component } from 'react';
import Input from '../Input';

export default class PasswordInput extends Component {
  render() {
    return (
      <Input type='password' {...this.props}/>
    );
  }
}
