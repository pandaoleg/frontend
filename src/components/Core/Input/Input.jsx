import React, { Component } from 'react';
import cn from 'cn-decorator';
import './Input.scss';

@cn('input-container')
export default class Input extends Component {
  state = {
    value: ''
  }

  /**
   * Get input's properties
   */
  _getInputProperties = () => {
    return {
      defaultValue: this.props.defaultValue || '',
      type: this.props.type || 'text',
      className: 'input ' + (this.props.className || ''),
      onChange: (e) => {
        if (this.props.onChange) this.props.onChange(e);
        else this.setState({value: e.target.value});
      },
      id: this.props.id,
      placeholder: this.props.placeholder
    }
  };

  render(cn) {
    return (
      <div>
        <div className={cn()}>
          <label className={cn('label')}>{this.props.label}</label>
          <input ref={this.props.inputRef} {...this._getInputProperties()}/>
          {this.props.icon}
        </div>
      </div>
    );
  }
}