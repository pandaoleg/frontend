import React, { Component } from 'react';
import Loader from 'react-loaders'
import './Button.scss';

export default class Button extends Component {
  render() {
    const getContent = () => {
      console.log(this.props.isLoading);
      return this.props.isLoading ? (<Loader type='line-scale-pulse-out' active/>) : `${this.props.icon || ''}${this.props.text || this.props.children || ''}`;
    };

    return (
      <button className={'button ' + (this.props.className || '')}>
        {getContent()}
      </button>
    );
  }
}
