import React, { Component } from 'react';
import '../Button';
import Button from '../Button';

export default class BlueButton extends Component {
  render() {
    return (
      <Button {...this.props} className='button--blue'>{this.props.text || this.props.children || ''}</Button>
    );
  }
}