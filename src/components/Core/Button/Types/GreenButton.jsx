import React, { Component } from 'react';
import '../Button';
import Button from '../Button';

export default class GreenButton extends Component {
  render() {
    return (
      <Button {...this.props} className='button--green'>{this.props.text || this.props.children || ''}</Button>
    );
  }
}