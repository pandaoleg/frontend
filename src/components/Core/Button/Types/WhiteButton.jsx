import React, { Component } from 'react';
import '../Button';
import Button from '../Button';

export default class WhiteButton extends Component {
  render() {
    return (
      <Button {...this.props} className='button--white'>{this.props.text || this.props.children || ''}</Button>
    );
  }
}