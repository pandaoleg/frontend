import React, { Component } from 'react';
import '../Button';
import Button from '../Button';

export default class RedButton extends Component {
  render() {
    return (
      <Button {...this.props} className='button--red'>{this.props.text || this.props.children || ''}</Button>
    );
  }
}