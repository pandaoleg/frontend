import React, { Component } from 'react';
import '../Button';
import Button from '../Button';

export default class PurpleButton extends Component {
  render() {
    return (
      <Button {...this.props} className='button--purple'>{this.props.text || this.props.children || ''}</Button>
    );
  }
}