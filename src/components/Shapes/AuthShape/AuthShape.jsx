import React, { Component } from 'react';
import cn from 'cn-decorator';
import './AuthShape.scss';

// -------- Import svgs ------
import { Star, Square, Circle } from 'react-feather'
import TopRightVector from '../../../img/auth/TopRight.svg';
import BottomLeftVector from '../../../img/auth/BottomLeft.svg';

@cn('auth-vectors')
export default class AuthShape extends Component {
  render(cn) {
    return (
      <div className={cn()}>
        <img src={TopRightVector} alt='vector'/>
        <img src={BottomLeftVector} alt='vector'/>
        
        <Star />
        <Star />
        <Star />
        <Star />
        <Star />

        <Square />
        <Square />
        <Square />
        <Square />
        <Square />

        <Circle />
        <Circle />
      </div>
    );
  }
}
