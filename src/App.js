import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router';
import { connect } from 'react-redux';
import {withRouter} from 'react-router-dom';
import './styles/app.scss';

// ------ IMPORT PAGES -------
import authorize from './view/authorize';
import Player from './view/player';

const userActions = require('./actions/userActions');

class App extends Component {
  componentDidMount() {
    if (this.props.user.authorized) this.props.validate(this.props.user);
  }

  render() {
    return (
      <div className="App">
        <Switch>
          <Route path={`${process.env.BASE_URL}login/`} component={authorize} />
          <Route path={`${process.env.BASE_URL}player/`} render={(props) => {
            if (this.props.user.authorized) return (<Player />)
            else return (<Redirect to='/login/' />)
          }} />
        </Switch>
      </div>
    );
  }
}

export default withRouter(connect(
  state => ({
    user: state.user
  }),
  
  dispatch => ({
    validate: (user) => {
      dispatch(userActions.validateHash(user));
    }
  })
)(App));
