import { combineReducers } from 'redux';

// ----------- Import all reducers -----------
import userReducer from './user';

export default combineReducers({
  user: userReducer
});