const initialState = {
  authorized: false,
  info: {}
};

export default function userReducer(state = initialState, action) {
  if (action.type === 'AUTH_SUCCESS') return Object.assign({}, state, action.payload);
  if (action.type === 'LOG_OUT') {
    window.location.href = `${process.env.BASE_URL}login/`;
    return initialState;
  }
  return state;
}
